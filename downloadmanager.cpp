#include "downloadmanager.h"

DownloadManager::DownloadManager(QObject *parent) : QObject(parent)
{
    connect(&m_Manager, &QNetworkAccessManager::finished, this, &DownloadManager::onDownloadFinished);
}

void DownloadManager::enqueueDownload(const QUrl& url)
{
    m_DownloadUrls.enqueue(url);
}

void DownloadManager::download()
{
    if (m_DownloadUrls.isEmpty()) {
        emit completed();
        return;
    }

    QUrl url = m_DownloadUrls.dequeue();

    QNetworkRequest request(url);
    request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);

    QNetworkReply *pReply = m_Manager.get(request);
    connect(pReply, &QNetworkReply::downloadProgress, this, &DownloadManager::onDownloadProgress);
}

void DownloadManager::onDownloadFinished(QNetworkReply *pReply)
{
    if (pReply->error())
    {
        qDebug() << "Download failed: " << pReply->errorString();
        return;
    }

    emit finished(pReply->url(), pReply->readAll());
    pReply->deleteLater();

    download();
}

void DownloadManager::onDownloadProgress(qint64 current, qint64 max)
{
    emit progress(current, max);
}

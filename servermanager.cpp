#include <QMessageBox>

#include "servermanager.h"

ServerManager::ServerManager(QObject *parent) : QObject(parent)
{
}

ServerManager::~ServerManager()
{
    closesocket(m_Socket);
    WSACleanup();
}

bool ServerManager::start(QThread &thread)
{
    if (!initWinSock())
        return false;

    connect(&thread, &QThread::started, this, &ServerManager::listener);
    connect(&thread, &QThread::finished, &thread, &QThread::deleteLater);

    m_Running = true;
    moveToThread(&thread);
    thread.start();

    return true;
}

void ServerManager::stop()
{
    m_Mutex.lock();
    m_Running = false;
    m_Mutex.unlock();
}

void ServerManager::clear()
{
    m_Mutex.lock();
    m_Requests.clear();
    m_Mutex.unlock();
}

QVector<ServerAddress> ServerManager::failRequests()
{
    QVector<ServerAddress> requests;

    m_Mutex.lock();
    for (auto request : m_Requests)
        requests.append(request.address);
    m_Mutex.unlock();

    return requests;
}

void ServerManager::info(ServerAddress addres)
{
    send(addres, "GOi");
}

void ServerManager::ping(ServerAddress addres)
{
    send(addres, "GOp");
}

void ServerManager::details(ServerAddress addres)
{
    send(addres, "GOd");
}

void ServerManager::send(ServerAddress &addres, QString packet)
{
    m_Mutex.lock();

    struct sockaddr_in si_server;
    si_server.sin_family = AF_INET;
    si_server.sin_port = htons(addres.Port);
    si_server.sin_addr.S_un.S_addr = inet_addr(addres.IP.toStdString().c_str());

    sendto(m_Socket, packet.toStdString().c_str(), packet.size(), 0, (struct sockaddr *)&si_server, sizeof(si_server));

    Request request;
    request.address = addres;
    request.packet = packet;
    request.time = QDateTime::currentMSecsSinceEpoch();
    m_Requests.append(request);

    m_Mutex.unlock();
}

qint64 ServerManager::exists(ServerAddress &address, QString packet)
{
    int idx = 0;
    for (auto& request : m_Requests)
    {
        if (request.address.IP == address.IP &&
            request.address.Port == address.Port &&
            request.packet == packet)
        {
            m_Requests[idx] = m_Requests.last();
            m_Requests.pop_back();

            return request.time;
        }

        ++idx;
    }

    return -1;
}

bool ServerManager::initWinSock()
{
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
    {
        QMessageBox::warning(nullptr, "Network", QString("Cannot initialize WinSock 2.2! Error code: %1").arg(WSAGetLastError()));
        return false;
    }

    if ((m_Socket = socket(AF_INET, SOCK_DGRAM, 0)) == INVALID_SOCKET)
    {
        QMessageBox::warning(nullptr, "Network", "Cannot create Socket!");
        return false;
    }

    u_long nMode = 1; // 1: NON-BLOCKING
    if (ioctlsocket(m_Socket, FIONBIO, &nMode) == SOCKET_ERROR)
    {
        QMessageBox::warning(nullptr, "Network", "Cannot set Socket to non-blocking state!");
        return false;
    }

    return true;
}

void ServerManager::listener()
{
    char buffer[512] = { 0 };

    struct sockaddr_in si_server;
    int sockLen = sizeof(si_server);

    memset((char*)&si_server, 0, sizeof(si_server));
    si_server.sin_family = AF_INET;

    while (m_Running)
    {
        memset(buffer, 0, 512);

        int len = recvfrom(m_Socket, buffer, 512, 0, (struct sockaddr*)&si_server, &sockLen);
        if (len <= 0)
        {
            QThread::msleep(5);
            continue;
        }

        ServerAddress address;
        address.IP = inet_ntoa(si_server.sin_addr);
        address.Port = ntohs(si_server.sin_port);

        // Parse packets
        if (memcmp("GO", buffer, 2) == 0)
        {
            // Prevent form packets that aren't response
            // for sended request
            qint64 time = exists(address, QString("GO") + buffer[2]);
            // Server wasn't on list
            if (time == -1) continue;

            switch (buffer[2])
            {
            case 'p':
                {
                    PacketPing packet = PacketPing::parse(address, buffer, len);
                    if (packet.address.isUnassigned()) continue;
                    packet.ping = QDateTime::currentMSecsSinceEpoch() - time;

                    emit serverPing(packet);
                }
                break;

            case 'i':
                {
                    PacketInfo packet = PacketInfo::parse(address, buffer, len);
                    if (packet.address.isUnassigned()) continue;
                    packet.ping = QDateTime::currentMSecsSinceEpoch() - time;

                    emit serverInfo(packet);
                }
                break;

            case 'd':
                {
                    PacketDetails packet = PacketDetails::parse(address, buffer, len);
                    if (packet.address.isUnassigned()) continue;

                    emit serverDetails(packet);
                }
                break;
            }
        }
    }
}

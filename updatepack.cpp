#include <QBuffer>
#include <QIODevice>
#include <QDir>
#include <JlCompress.h>

#include "updatepack.h"
#include "config.h"

UpdatePack::UpdatePack(bool isMilestone, const QByteArray& data) :
    m_IsMilestone(isMilestone)
{
    // Buffer in memory
    if (!m_Buffer.open(QIODevice::WriteOnly))
        return;

    m_Buffer.write(data);
    m_Buffer.close();
}

bool UpdatePack::extract()
{
    if (m_IsMilestone)
    {
        createInstallerFile();
        return true;
    }

    QuaZip zipFile(&m_Buffer);
    if (!zipFile.open(QuaZip::mdUnzip))
        return false; // Cannon open to unzip

    if (!zipFile.goToFirstFile())
        return false; // No files

    do
    {
        QString filePath = Config::get().getGameDir() + "/Multiplayer/update/" + zipFile.getCurrentFileName();
        createZipFile(zipFile, filePath);
    } while (zipFile.goToNextFile());

    zipFile.close();
    if (zipFile.getZipError() != 0)
    {
        // ERROR
    }

    return true;
}

void UpdatePack::createUpdateDir()
{
    QDir dir = Config::get().getGameDir() + "/Multiplayer/update/";
    dir.mkpath(".");
}

#include <QDebug>

bool UpdatePack::createInstallerFile()
{
    if (!m_Buffer.open(QIODevice::ReadOnly))
        return false;

    QString file_location = Config::get().getGameDir() + "/Multiplayer/update/milestone.exe";
    QFile outFile(file_location);

    qDebug() << file_location;

    bool success = outFile.open(QIODevice::WriteOnly);
    if (success && !copyData(m_Buffer, outFile))
        success = false;

    m_Buffer.close();
    return success;
}

bool UpdatePack::createZipFile(QuaZip &zipFile, QString filePath)
{
    QuaZipFile inFile(&zipFile);
    if (!inFile.open(QIODevice::ReadOnly) || inFile.getZipError() != UNZ_OK)
        return false;

    // Create dirpath if not exist
    if (filePath.at(filePath.size() - 1) == '/')
    {
        QDir dir(filePath);
        if (!dir.exists())
            dir.mkpath(".");

        return true;
    }

    QFile outFile;
    outFile.setFileName(filePath);

    if (!outFile.open(QIODevice::WriteOnly))
        return false;

    if (!copyData(inFile, outFile) || inFile.getZipError() != UNZ_OK)
    {
        outFile.close();
        return false;
    }

    outFile.close();
    inFile.close();

    return inFile.getZipError() == UNZ_OK;
}

bool UpdatePack::copyData(QIODevice &inFile, QIODevice &outFile)
{
    while (!inFile.atEnd()) {
        char buf[4096];
        qint64 readLen = inFile.read(buf, 4096);
        if (readLen <= 0)
            return false;
        if (outFile.write(buf, readLen) != readLen)
            return false;
    }
    return true;
}

#include <QtGlobal>
#include <QKeyEvent>

#include "servertreewidgetitem.h"
#include "servertreewidget.h"

ServerTreeWidget::ServerTreeWidget(QWidget *parent) :
    QTreeWidget(parent)
{
}

void ServerTreeWidget::init()
{
    setSortingEnabled(true);

    // Remove tree intendation (invisible spacing on first column)
    setIndentation(0);

    QStringList labels;
    labels << "Hostname" << "IP" << "Port" << "Version" << "Players" << "Ping";
    setHeaderLabels(labels);
    sortByColumn(4, Qt::DescendingOrder); // Sort by players count

    setContextMenuPolicy(Qt::CustomContextMenu);
    setColumnWidth(0, 200);
    setColumnWidth(2, 50);
    setColumnWidth(3, 80);
    setColumnWidth(4, 70);
}

void ServerTreeWidget::insert(const PacketInfo &packet)
{
    ServerTreeWidgetItem *row = new ServerTreeWidgetItem(this);

    row->setHostname(packet.hostName);
    row->setAddress(packet.address);

    if (packet.request)
    {
        row->setVersion("?");
        row->setPlayers("?");
        row->setPing("?");
    }
    else
    {
        row->setVersion(packet.version, packet.codeName);
        row->setPlayers(packet.players, packet.maxSlots);
        row->setPing(packet.ping);
    }

    this->addTopLevelItem(row);
}

void ServerTreeWidget::update(const PacketInfo &packet, ServerTreeWidgetItem* row)
{
    row = row ? row : findRow(packet.address);
    if (row == nullptr)
        return;

    row->setHostname(packet.hostName);

    if (packet.request)
    {
        row->setVersion("?");
        row->setPlayers("?");
        row->setPing("?");
    }
    else
    {
        row->setVersion(packet.version, packet.codeName);
        row->setPlayers(packet.players, packet.maxSlots);
        row->setPing(packet.ping);
    }
}

ServerTreeWidgetItem* ServerTreeWidget::findRow(const ServerAddress& address)
{
    for (int i = 0; i < this->topLevelItemCount(); ++i) {
        ServerTreeWidgetItem* row = static_cast<ServerTreeWidgetItem*>(this->topLevelItem(i));
        const ServerAddress& rowAddress = row->getAddress();

        if (rowAddress.IP == address.IP && rowAddress.Port == address.Port)
            return row;
    }

    return nullptr;
}

void ServerTreeWidget::keyPressEvent(QKeyEvent* event)
{
    QTreeWidget::keyPressEvent(event);

    switch (event->key())
    {
        case Qt::Key_Return:
        case Qt::Key_Enter:
            emit enterKeyPressed(currentItem());
            break;

        case Qt::Key_Insert:
            emit insertKeyPressed();
            break;

        case Qt::Key_Delete:
            emit deleteKeyPressed();
            break;

        case Qt::Key_Left:
        case Qt::Key_Minus:
        {
            QModelIndex currentIndex = this->currentIndex();
            QModelIndex newIndex = model()->index(currentIndex.row(), qBound(0, currentIndex.column() - 1, columnCount() - 1));
            this->setCurrentIndex(newIndex);
            break;
        }

        case Qt::Key_Right:
        case Qt::Key_Plus:
        {
            QModelIndex currentIndex = this->currentIndex();
            QModelIndex newIndex = model()->index(currentIndex.row(), qBound(0, currentIndex.column() + 1, columnCount() - 1));
            this->setCurrentIndex(newIndex);
            break;
        }
    }        
}

#ifndef SERVERADDRESS_H
#define SERVERADDRESS_H

#include <QString>

class ServerAddress
{
public:
    static ServerAddress parse(QString ipAddr);
    bool isUnassigned() const;
    QString toString() const;

    QString IP;
    ushort Port;
};

#endif // SERVERADDRESS_H

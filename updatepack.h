#ifndef UPDATEPACK_H
#define UPDATEPACK_H

#include <QBuffer>
#include <quazip.h>

class UpdatePack
{
public:
    UpdatePack(bool isMilestone, const QByteArray& data);

    bool extract();
    static void createUpdateDir();

private:
    bool createInstallerFile();
    bool createZipFile(QuaZip& zipFile, QString filePath);
    bool copyData(QIODevice &inFile, QIODevice &outFile);

    QBuffer m_Buffer;
    bool m_IsMilestone;
};

#endif // UPDATEPACK_H

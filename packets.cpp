#include "packets.h"

#include <QDebug>

PacketPing PacketPing::parse(const ServerAddress& address, char *buffer, int len)
{
    Q_UNUSED(len);
    Q_UNUSED(buffer);

    PacketPing packet;
    packet.address = address;

    return packet;
}

PacketInfo PacketInfo::parse(const ServerAddress& address, char *buffer, int len)
{
    uchar version = buffer[3];

    PacketInfo packet;
    packet.address = address;

    // Server version
    packet.version.major = buffer[4];
    packet.version.minor = buffer[5];
    packet.version.patch = buffer[6];

    if (version == 0x1)
    {
        // Slots
        packet.players = static_cast<uchar>(buffer[7]);
        packet.maxSlots = static_cast<uchar>(buffer[8]);

        // Read Host name
        char sHostName[34] = { 0 };
        memcpy(sHostName, &buffer[9], len - 9);
        packet.hostName = sHostName;

        return packet;
    }
    else if (version == 0x2)
    {
        // Read code name
        char sCodeName[16] = { 0 };
        strcpy_s(sCodeName, 16, &buffer[7]);
        packet.codeName = sCodeName;

        // Slots
        packet.players = static_cast<uchar>(buffer[23]);
        packet.maxSlots = static_cast<uchar>(buffer[24]);

        // Read Host name
        char sHostName[34] = { 0 };
        memcpy(sHostName, &buffer[25], len - 25);
        packet.hostName = sHostName;

        return packet;
    }
    else if (version == 0x3)
    {
        // Read code name
        char sCodeName[16] = { 0 };
        strcpy_s(sCodeName, 16, &buffer[7]);
        packet.codeName = sCodeName;

        // Slots
        packet.players = *reinterpret_cast<uint16_t*>(buffer + 23);
        packet.maxSlots = *reinterpret_cast<uint16_t*>(buffer + 25);

        // Read Host name
        char sHostName[34] = { 0 };
        memcpy(sHostName, &buffer[27], len - 27);
        packet.hostName = sHostName;

        return packet;
    }

    return PacketInfo();
}

PacketInfo PacketInfo::pinging(const ServerAddress& address)
{
    PacketInfo packet;

    packet.request = true;
    packet.address = address;
    packet.hostName = "Pinging...";

    return packet;
}

PacketInfo PacketInfo::unknown(const ServerAddress& address)
{
    PacketInfo packet;

    packet.request = true;
    packet.address = address;
    packet.hostName = "Server didn't respond";
    
    return packet;
}

PacketDetails PacketDetails::parse(const ServerAddress& address, char *buffer, int len)
{
    uchar version = buffer[3];

    if (version == PACKET_VERSION)
    {
        PacketDetails packet;
        packet.address = address;

        // Read world
        int worldLen = buffer[4];
        char sWorld[34] = { 0 };
        memcpy(sWorld, &buffer[5], worldLen);
        packet.world = sWorld;

        // Read description
        char sDescription[410] = { 0 };
        memcpy(sDescription, &buffer[5 + worldLen], len - 5 - worldLen);
        packet.description = sDescription;

        return packet;
    }

    return PacketDetails();
}

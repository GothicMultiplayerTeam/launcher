#ifndef HTTPMANAGER_H
#define HTTPMANAGER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QUrl>

class HttpManager : public QObject
{
    Q_OBJECT

public:
    explicit HttpManager(QObject *parent = nullptr);
    void post(QUrl url, QJsonDocument doc);
    void get(QUrl url);

signals:
    void finished(QUrl url, QJsonDocument doc);

private slots:
    void onRequestFinished(QNetworkReply *pReply);

private:
    QNetworkAccessManager m_Manager;
    QList<QNetworkReply*> m_Downloads;
};

#endif // HTTPMANAGER_H

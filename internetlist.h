#ifndef INTERNETLIST_H
#define INTERNETLIST_H

#include "serverlist.h"
#include "httpmanager.h"

class ServerTreeWidgetItem;

class InternetList : public ServerList
{
    Q_OBJECT

public:
    InternetList(Ui::Launcher *ui);
    void init();
    void refreshAll();

signals:
    void favoriteAdded(ServerAddress address);

private slots:
    void onConnect();
    void onFavoriteAdded();
    void onTimeout() override;
    void onRequestFinished(QUrl url, QJsonDocument doc);

private:
    void setupMenu();

    HttpManager m_Http;
};

#endif // INTERNETLIST_H

#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <QObject>
#include <QQueue>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>

class DownloadManager : public QObject
{
    Q_OBJECT

public:
    explicit DownloadManager(QObject *parent = nullptr);
    void enqueueDownload(const QUrl& url);
    void download();

signals:
    void finished(QUrl url, QByteArray data);
    void progress(qint64 current, qint64 max);
    void completed();

private slots:
    void onDownloadFinished(QNetworkReply *pReply);
    void onDownloadProgress(qint64 current, qint64 max);

private:
    QNetworkAccessManager m_Manager;
    QQueue<QUrl> m_DownloadUrls;
};

#endif // DOWNLOADMANAGER_H

#include "servertreewidgetitem.h"

ServerTreeWidgetItem::ServerTreeWidgetItem(QTreeWidget *treeview, int type) :
    QTreeWidgetItem(treeview, type)
{

}

void ServerTreeWidgetItem::setHostname(const QString& hostName)
{
    setText(0, hostName);
}

void ServerTreeWidgetItem::setAddress(const ServerAddress& address)
{
    m_Address = address;

    setText(1, address.IP);
    setText(2, QString::number(address.Port));
}

void ServerTreeWidgetItem::setVersion(const QString& version)
{
    setText(3, version);
}

void ServerTreeWidgetItem::setVersion(const Version& version, QString codeName)
{
    m_Version = version;

    QString versionCombined = QString("%1.%2.%3")
            .arg(version.major)
            .arg(version.minor)
            .arg(version.patch);

    if (!codeName.isEmpty())
        versionCombined += " " + codeName;

    setText(3, versionCombined);
}

void ServerTreeWidgetItem::setPlayers(const QString& version)
{
    setText(4, version);
}

void ServerTreeWidgetItem::setPlayers(int players, int maxSlots)
{
    m_Players = players;
    setText(4, QString("%1/%2").arg(players).arg(maxSlots));
}

void ServerTreeWidgetItem::setPing(const QString& ping)
{
    setText(5, ping);
}

void ServerTreeWidgetItem::setPing(int ping)
{
    m_Ping = ping;
    setText(5, QString::number(ping));
}

bool ServerTreeWidgetItem::operator<(const QTreeWidgetItem &other) const
{
    auto otherItem = reinterpret_cast<const ServerTreeWidgetItem*>(&other);
    switch (treeWidget()->sortColumn())
    {
        case 2: return getAddress().Port < otherItem->getAddress().Port;
        case 4: return getPlayers() < otherItem->getPlayers();
        case 5: return getPing() < otherItem->getPing();
        default: return QTreeWidgetItem::operator<(other);
    }
}
